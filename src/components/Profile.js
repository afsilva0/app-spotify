import React from "react";

import { Form, Col, Row, Card } from "react-bootstrap";

class Profile extends React.Component {
  state = {
    respcadena: {},
    followers: {},
  };

  componentDidMount = () => {
    this.perfil();
  };

  perfil = async () => {
    let url = "https://api.spotify.com/v1/me";

    await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ respcadena: responseJson });
        this.setState({ explicit_content: responseJson.explicit_content });
        this.setState({ followers: responseJson.followers });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    return (
      <div>
        <Card xs={12}>
          <Card.Body>
            <Card.Title>INFORMACIÓN DE PERFIL</Card.Title>
            <Form>
              <Form.Group as={Row}>
                <Form.Label column sm="3">
                  Usuario:
                </Form.Label>
                <Col sm="9">
                  <Form.Control
                    readOnly
                    value={this.state.respcadena.display_name}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row}>
                <Form.Label column sm="3">
                  Correo electrónio:
                </Form.Label>
                <Col sm="9">
                  <Form.Control readOnly value={this.state.respcadena.email} />
                </Col>
              </Form.Group>

              <Form.Group as={Row}>
                <Form.Label column sm="3">
                  Pais:
                </Form.Label>
                <Col sm="9">
                  <Form.Control
                    readOnly
                    value={this.state.respcadena.country}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row}>
                <Form.Label column sm="3">
                  Seguidores:
                </Form.Label>
                <Col sm="9">
                  <Form.Control readOnly value={this.state.followers.total} />
                </Col>
              </Form.Group>
            </Form>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default Profile;
