import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Toolbar from "@material-ui/core/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import ReactDOM from "react-dom";
import { Button } from "react-bootstrap";
import MenuIcon from "@material-ui/icons/Menu";
import Hidden from "@material-ui/core/Hidden";
import Playlist from "./Playlist";
import Profile from "./Profile";

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      bar: 0,
    };
  }

  componentDidMount() {
    ReactDOM.render(<Profile />, document.getElementById("contendorData"));
  }

  iniciosesion = () => {
    document.location("/");
  };

  cerrarSesion = () => {
    localStorage.removeItem("token");
    window.location.href = "/";
  };

  render() {
    return (
      <div>
        <React.Fragment>
          <AppBar
            component="div"
            color="primary"
            position="static"
            elevation={0}
          >
            <br></br>

            <Toolbar>
              <Grid container alignItems="center" spacing={1}>
                <Hidden smUp>
                  <Grid item>
                    <IconButton
                      color="inherit"
                      aria-label="open drawer"
                      onClick={this.props.onDrawerToggle}
                    >
                      <MenuIcon />
                    </IconButton>
                  </Grid>
                </Hidden>
                <Grid item xs></Grid>
                <Grid item></Grid>

                <Tooltip title="Cerrar Sesion">
                  <Button
                    className="outline-primary"
                    onClick={this.cerrarSesion}
                  >
                    Cerrar Sesión
                  </Button>
                </Tooltip>
              </Grid>
            </Toolbar>
            <br></br>

            <Grid item xs>
              <Typography color="inherit" variant="h5">
                Menú
              </Typography>
            </Grid>
            <br></br>
          </AppBar>
          <AppBar color="primary" position="static" elevation={0}>
            <Tabs
              value={this.state.bar}
              textColor="inherit"
              variant="scrollable"
              scrollButtons="auto"
            >
              <Tab
                textColor="inherit"
                label="Perfil"
                title="Perfil"
                onClick={() => {
                  this.setState({ bar: 0 });
                  ReactDOM.render(
                    <Profile />,
                    document.getElementById("contendorData")
                  );
                }}
              ></Tab>
              <Tab
                textColor="inherit"
                label="Playlist "
                title="Playlist"
                onClick={() => {
                  this.setState({ bar: 1 });
                  ReactDOM.render(
                    <Playlist />,
                    document.getElementById("contendorData")
                  );
                }}
              ></Tab>
            </Tabs>
          </AppBar>
        </React.Fragment>
      </div>
    );
  }
}

export default Header;
