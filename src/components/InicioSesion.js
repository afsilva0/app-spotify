import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Copyright from "../Utilitys/Copyright";
import Container from "@material-ui/core/Container";
import Navegador from "./Navegador";
import { Alert } from "react-bootstrap";
import { withStyles } from "@material-ui/core/styles";

const useStyles = (theme) => ({
  paper: {
    marginTop: "15%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },

  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

const validate = (values) => {
  const errors = {};

  if (!values.token) {
    errors.campos_obligatorios = "El Token es necesario para ingreso";
  }

  return errors;
};

class InicioSesion extends React.Component {
  constructor() {
    super();

    this.state = {
      token: "",
      errors: {},
      show: 0,
    };
  }

  componentDidMount() {
    let token = localStorage.getItem("token");
    if (token) {
      window.location.href = "/inicio";
    }
  }

  handleInput = async (e) => {
    // obtiene la informacion de los campos de texto y los set en os estados
    const { value, name } = e.target;
    await this.setState({
      [name]: value,
    });
  };

  handleSubmit = async () => {
    const { errors, ...sinErrors } = this.state;
    const result = validate(sinErrors);

    this.setState({ errors: result });

    //Esta linea de codigo valida si no existe errores y crea localStorage del token
    if (!Object.keys(result).length) {
      localStorage.setItem("token", this.state.token);
      window.location.href = "/Inicio";
    }
  };

  validarformulario = (validar) => {
    switch (validar) {
      case 0:
        return;
      case 1:
        return <Alert variant="danger">Ingreso inválido</Alert>;
      case 2:
        return <Alert variant="danger"> Contraseña incorrecta</Alert>;
      case 3:
        return <Alert variant="success"> Bienvenido </Alert>;
      default:
        throw new Error("Unknown Reunion");
    }
  };

  render() {
    const { errors } = this.state;
    return (
      <div>
        <Navegador />
        <Container component="main" maxWidth="sm">
          <CssBaseline />
          <div className={this.props.classes.paper}>
            <Avatar className={this.props.classes.avatar}></Avatar>
            <Typography component="h1" variant="h5">
              Iniciar Sesion
            </Typography>
            <div className={this.props.classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="token"
                label="Token"
                type="text"
                name="token"
                autoComplete="token"
                autoFocus
                onChange={this.handleInput}
                inputProps={{
                  maxLength: 300,
                }}
              />

              <br></br>
              {errors.campos_obligatorios && (
                <Alert variant="danger">{errors.campos_obligatorios} </Alert>
              )}
              {this.validarformulario(this.state.show)}
              <Button
                type="button"
                fullWidth
                variant="contained"
                color="primary"
                className={this.props.classes.submit}
                onClick={this.handleSubmit}
              >
                Iniciar Sesión
              </Button>
            </div>
          </div>
          <Box mt={8}>
            <Copyright />
          </Box>
        </Container>
      </div>
    );
  }
}

export default withStyles(useStyles)(InicioSesion);
