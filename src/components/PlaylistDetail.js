import React from "react";

import { Button, Table, ButtonGroup, Image, Modal } from "react-bootstrap";

class PlaylistDeail extends React.Component {
  state = {
    respcadena: [],
    respcancion: [],
    items: [],
    showModalCancion: false,
  };

  componentDidMount = () => {
    this.playlist(this.props.id);
  };

  modalCloseCancion = () => {
    this.setState({ showModalCancion: false });
  };

  modalShowCancion = () => {
    this.setState({ showModalCancion: true });
  };

  playlist = async (id) => {
    let url = "https://api.spotify.com/v1/playlists/" + id + "/tracks";
    await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ respcadena: responseJson });
        this.setState({ items: responseJson.items });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  cancion = async (id, images) => {
    this.setState({ imagenCancion: images });
    console.log(id);
    let url = "https://api.spotify.com/v1/tracks/" + id + "";
    await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ respcancion: responseJson });
      })
      .catch((error) => {
        console.log(error);
      });
    this.modalShowCancion();
  };

  enviarCancion = () => {
    window.location.href = "spotify:track:1cMTzjQOTt3wVAbFgTGPYN";
  };

  eliminarCancion = async (uri) => {
    let playlist_id = this.props.id;

    let data = {
      tracks: [
        {
          uri: uri,
          positions: [0],
        },
      ],
    };
    let url = "https://api.spotify.com/v1/playlists/" + playlist_id + "/tracks";
    await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
      })
      .catch((error) => {
        console.log(error);
      });

    this.playlist(this.props.id);
  };

  render() {
    return (
      <div>
        <p>Cantidad Encontradas:{this.state.respcadena.total}</p>

        <Table
          responsive
          striped
          bordered
          hover
          variant="dark"
          style={{
            height: "auto",
            maxWidth: "100%",
          }}
        >
          <thead>
            <tr>
              <th></th>
              <th>Titulo</th>
              <th>Album</th>
              <th>Fecha Incorporación </th>
              <th>Acciones</th>
            </tr>
          </thead>
          {this.state.items.map((e) => {
            return (
              <tbody key={e.track.id}>
                <tr>
                  <td
                    style={{
                      width: "10%",
                      height: "auto",
                    }}
                  >
                    <Image
                      style={{
                        width: "100%",
                        height: "auto",
                      }}
                      src={e.track.album.images[0].url}
                      rounded
                    />
                  </td>
                  <td>{e.track.name}</td>
                  <td>{e.track.album.name}</td>
                  <td>{e.track.album.release_date}</td>

                  <td>
                    <ButtonGroup className="mx-2 my-2">
                      <Button
                        className=" btn btn-primary btn-xs"
                        onClick={() =>
                          this.cancion(e.track.id, e.track.album.images[0].url)
                        }
                      >
                        Reproduccir
                      </Button>
                    </ButtonGroup>

                    <ButtonGroup>
                      <Button
                        className=" btn btn-danger btn-xs"
                        onClick={() => this.eliminarCancion(e.track.uri)}
                      >
                        Eliminar
                      </Button>
                    </ButtonGroup>
                  </td>
                </tr>
              </tbody>
            );
          })}
        </Table>

        <div>
          <Modal
            show={this.state.showModalCancion}
            onHide={this.modalCloseCancion}
          >
            <Modal.Header closeButton>
              <Modal.Title>{this.state.respcancion.name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                <a href="spotify:track:1cMTzjQOTt3wVAbFgTGPYN" rel="noopener">
                  Reproducior Cancion en spotify
                </a>
              </div>
              <Image
                style={{
                  width: "70%",
                  height: "auto",
                }}
                src={this.state.imagenCancion}
                rounded
              />
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.modalCloseCancion}>Aceptar</Button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    );
  }
}

export default PlaylistDeail;
