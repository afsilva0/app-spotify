import React from "react";

import { Nav, Navbar, Form, ButtonGroup } from "react-bootstrap";
import ReactDOM from "react-dom";
import InicioSesion from "./InicioSesion";

import { Link } from "react-router-dom";

class Navegador extends React.Component {
  iniciosesion = () => {
    ReactDOM.render(
      <InicioSesion></InicioSesion>,
      document.getElementById("root")
    );
  };
  render() {
    return (
      <div>
        <Navbar bg="dark" variant="dark">
          <Link className="btn btn-dark" to="/">
            SPOTIFY
          </Link>
          <Nav className="mr-auto"></Nav>
          <Form inline>
            <ButtonGroup className="mr-2 " aria-label="Primer group">
              <Link className="btn  btn-outline-light" to="/iniciosesion">
                Iniciar
              </Link>
            </ButtonGroup>
          </Form>
        </Navbar>
      </div>
    );
  }
}

export default Navegador;
