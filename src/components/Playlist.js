import React from "react";

import PlaylistDeail from "./PlaylistDetail";

import {
  Button,
  Table,
  ButtonGroup,
  Col,
  Row,
  Form,
  Modal,
  Alert,
} from "react-bootstrap";

const validate = (values) => {
  const errors = {};

  if (!values.nombre || !values.descripcion) {
    errors.campos_obligatorios = "El Todos los campos son obligatorios";
  }

  return errors;
};

class Playlist extends React.Component {
  state = {
    respcadena: [],
    id_perfil: "",
    id: "",
    showModalPlaylist: false,
    nombre: "",
    descripcion: "",
    tipo: "false",
    errors: {},
  };

  componentDidMount = () => {
    this.playlist();
    this.perfil();
  };

  handleInput = async (e) => {
    const { value, name } = e.target;
    await this.setState({ [name]: value });

    console.log(this.state);
  };

  playlist = async () => {
    let url = "https://api.spotify.com/v1/me/playlists";
    await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ respcadena: responseJson.items });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  enviar = (id, images) => {
    this.setState({ id: id });
    this.setState({ Images: images });
    /*  this.setState((states) => {
      return { id: id };
    }); */
    /*    this.verPlaylist(id, JSON.stringify(images)); */
    // console.log(images[0]);
  };

  verPlaylist = (id, images) => {
    //  console.log(images);
    if (id) {
      return <PlaylistDeail id={id} images={images}></PlaylistDeail>;
    } else {
      return <div>No hay Playlist para ver </div>;
    }
  };

  modalClosePlaylist = () => {
    this.setState({ showModalPlaylist: false });
    this.limpiarDatos();
  };

  modalShowPlaylist = () => {
    this.setState({ showModalPlaylist: true });
  };

  perfil = async () => {
    let url = "https://api.spotify.com/v1/me";
    await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ id_perfil: responseJson.id });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  crearPlaylist = async () => {
    const { errors, ...sinErrors } = this.state;
    const result = validate(sinErrors);

    this.setState({ errors: result });

    //Esta linea de codigo valida si no existe errores y hace el registro
    if (!Object.keys(result).length) {
      let user_id = this.state.id_perfil;

      let data = {
        name: this.state.nombre,
        description: this.state.descripticion,
        public: this.state.tipo,
      };
      let url = "https://api.spotify.com/v1/users/" + user_id + "/playlists";
      await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
        })
        .catch((error) => {
          console.log(error);
        });

      this.modalClosePlaylist();
      this.playlist();
    }
  };

  limpiarDatos = () => {
    this.setState({ nombre: "" });
    this.setState({ descripcion: "" });
  };

  render() {
    const { errors } = this.state;
    return (
      <div>
        <Row className="mb-5">
          <Col xs={6} sm={6}>
            <Form.Control
              type="text"
              placeholder="Buscar"
              onChange={this.handlebusqueda}
              id="buscar"
            />
          </Col>

          <ButtonGroup aria-label="group">
            <Button
              className="mr-2 "
              variant="primary"
              type="button"
              onClick={this.modalShowPlaylist}
            >
              Agregar
            </Button>
          </ButtonGroup>

          <Col xs={0} sm={2}></Col>
        </Row>

        <Table
          responsive
          striped
          bordered
          hover
          variant="dark"
          id="table-reunion"
        >
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Acciones</th>
            </tr>
          </thead>

          {this.state.respcadena.map((e) => {
            return (
              <tbody key={e.id}>
                <tr>
                  <td>{e.name}</td>
                  <td>
                    <ButtonGroup>
                      <Button
                        className=" btn btn-danger btn-xl"
                        onClick={() => this.enviar(e.id, e.images)}
                      >
                        Ver
                      </Button>
                    </ButtonGroup>
                  </td>
                </tr>
              </tbody>
            );
          })}
        </Table>

        <div>{this.verPlaylist(this.state.id, this.state.images)}</div>

        <Modal
          show={this.state.showModalPlaylist}
          onHide={this.modalClosePlaylist}
        >
          <Modal.Header closeButton>
            <Modal.Title> Crear Playlist</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <Modal.Body>
                <Form>
                  <Form.Group as={Row}>
                    <Form.Label column sm="3">
                      Nombre:
                    </Form.Label>
                    <Col sm="9">
                      <Form.Control name="nombre" onChange={this.handleInput} />
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column sm="3">
                      Descripticion:
                    </Form.Label>
                    <Col sm="9">
                      <Form.Control
                        name="descripcion"
                        onChange={this.handleInput}
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label column sm="3">
                      Tipo:
                    </Form.Label>
                    <Col sm="9">
                      <Form.Control
                        as="select"
                        name="tipo"
                        onChange={this.handleInput}
                        value={this.state.tipo}
                      >
                        <option value="false">Privada</option>
                        <option value="true">Publica</option>
                      </Form.Control>
                    </Col>
                  </Form.Group>
                </Form>
              </Modal.Body>
            </div>

            {errors.campos_obligatorios && (
              <Alert variant="danger">{errors.campos_obligatorios} </Alert>
            )}
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="btn btn-danger"
              onClick={this.modalClosePlaylist}
            >
              Cancelar
            </Button>
            <Button onClick={this.crearPlaylist}>Guardar Cambios</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default Playlist;
