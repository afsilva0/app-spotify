import React from "react";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Navegador from "./Navegador";
import { withStyles } from "@material-ui/core/styles";
import { ButtonGroup, Row, Col } from "react-bootstrap";

const useStyles = (theme) => ({
  background: {
    backgroundImage: `url("https://www.xtrafondos.com/wallpapers/estructura-abstracta-3d-gris-oscuro-3593.jpg")`,

    display: "block",
    margin: "auto",
    width: "300%",
    maxWidth: "100%",

    height: "auto",

    color: theme.palette.common.white,
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
  },
  button: {
    minWidth: 200,
  },
  h5Inicio: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
    padding: "0px  5% ",
    alignItems: "center",
    [theme.breakpoints.up("sm")]: {
      marginTop: theme.spacing(5),
    },
  },
  textInicio: {
    alignItems: "center",
  },

  more: {
    marginTop: theme.spacing(2),
  },

  imagenInformacion: {
    display: "block",
    margin: "auto",
    width: "100%",
    maxWidth: "100%",
    height: "auto",
    color: theme.palette.common.white,
    backgroundSize: "cover",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
  },
});

class InicioPagina extends React.Component {
  inicio = () => {
    window.location.href = "./InicioSesion";
  };
  

  render() {
    return (
      <div>
        <Navegador />
        <div className={this.props.classes.background}>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <Typography
            color="inherit"
            align="center"
            variant="h3"
            marked="center"
            className={this.props.classes.textInicio}
          >
            ¿Tienes ganas de escuchar algo?
          </Typography>
          <br></br>
          <br></br>

          <Typography
            color="inherit"
            align="center"
            variant="h5"
            className={this.props.classes.h5Inicio}
          >
            Escucha los mejores nuevos lanzamientos.
          </Typography>

          <Row>
            <Col xs={0} sm={4}></Col>
            <Col xs={12} sm={4}>
              <ButtonGroup className="mr-2 mt-3">
                <Button
                  color="secondary"
                  variant="contained"
                  size="large"
                  className={this.props.classes.button}
                  onClick={this.inicio}
                >
                  Iniciar
                </Button>
              </ButtonGroup>
            </Col>
            <Col xs={0} sm={4}></Col>
          </Row>
          <br></br>
          <br></br>
          <Typography
            variant="body2"
            color="inherit"
            className={this.props.classes.more}
          >
            Descubre la experiencia
          </Typography>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
        </div>
      </div>
    );
  }
}

export default withStyles(useStyles)(InicioPagina);
