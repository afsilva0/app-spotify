import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Inicio from "./components/Inicio";
import InicioPagina from "./components/InicioPagina";
import InicioSesion from "./components/InicioSesion";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={InicioPagina}></Route>
            <Route path="/Inicio" component={Inicio}></Route>
            <Route path="/InicioSesion" component={InicioSesion}></Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
